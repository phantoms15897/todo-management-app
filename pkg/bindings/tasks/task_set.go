package tasks

import (
	"backend/internal/tasks/infrastructure/ports/grpc"
	"backend/internal/tasks/repository"
	"backend/internal/tasks/usecase"
	"backend/pkg/dbutils"
	"database/sql"
	"errors"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/wire"
	"github.com/spf13/viper"
	gormoteltracing "gorm.io/plugin/opentelemetry/tracing"
)

var (
	moduleName = "tasks"
)

func CreateMySQLDB() repository.TasksDB {
	var dsn string
	v := viper.GetString(moduleName + ".mysql.dsn")
	if v != "" {
		dsn = v
	} else {
		panic(errors.New(moduleName + ".mysql.dsn is not set"))
	}
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}

	gormDB, err := dbutils.NewMySQLGorm(
		db,
		dbutils.WithDefaultLogger(),
		dbutils.WithPlugIn(gormoteltracing.NewPlugin()),
	)
	if err != nil {
		log.Fatalf("can not init gormDB ", err)
	}
	return gormDB
}

var TaskSet = wire.NewSet(
	grpc.NewTaskGRPCServer,
	usecase.NewTaskUsecase,
	repository.NewTaskRepository,
	CreateMySQLDB,
)
