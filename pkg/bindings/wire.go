//go:generate wire

//go:build wireinject
// +build wireinject

package bindings

import (
	"backend/pkg/bindings/profile"
	"backend/pkg/bindings/tasks"
	"backend/pkg/config"
	"backend/pkg/firebase"
	"backend/pkg/middleware"

	"github.com/google/wire"
	"google.golang.org/grpc"
)

func InitApp(
	cfg *config.Config,
	grpcServer *grpc.Server,
) (*App, error) {
	panic(wire.Build(
		tasks.TaskSet,
		profile.ProfileSet,
		middleware.NewAuthClient,
		firebase.SetupFirebase,
		New,
	))
}
