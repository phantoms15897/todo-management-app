package bindings

import (
	grpcProfile "backend/internal/profile/infrastructure/ports/grpc"
	grpcTask "backend/internal/tasks/infrastructure/ports/grpc"
)

type App struct {
	TaskGRPCServer    *grpcTask.TaskGRPCServer
	ProfileGRPCServer *grpcProfile.ProfileGRPCServer
}

func New(
	ProfileGRPCServer *grpcProfile.ProfileGRPCServer,
	TaskGRPCServer *grpcTask.TaskGRPCServer,
) *App {
	return &App{
		TaskGRPCServer:    TaskGRPCServer,
		ProfileGRPCServer: ProfileGRPCServer,
	}
}
