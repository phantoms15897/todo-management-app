package cmd

import (
	"backend/pkg/bindings"
	"backend/pkg/config"
	gen "backend/proto/gen"
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	gwruntime "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/automaxprocs/maxprocs"
	"google.golang.org/grpc"
)

var (
	cfgFile string
	cfgDir  string
)

// serverCmd represents the server command
var Cmd = &cobra.Command{
	Use:   "server",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// set GOMAXPROCS
		_, err := maxprocs.Set()
		if err != nil {
			log.Error("failed set max procs", err)
		}

		ctx, cancel := context.WithCancel(context.Background())

		cfg, err := config.NewConfig()
		fmt.Println(viper.Get("server.name"))
		if err != nil {
			log.Error("failed get config", err)
		}

		log.Info("⚡ init app ", "name ", cfg.Server.APP_NAME, " version ", cfg.Server.VERSION)

		// set up logrus
		log.SetFormatter(&log.JSONFormatter{})
		log.SetOutput(os.Stdout)
		log.SetLevel(log.InfoLevel)

		server := grpc.NewServer()

		go func() {
			defer server.GracefulStop()
			<-ctx.Done()
		}()

		_, err = bindings.InitApp(cfg, server)
		if err != nil {
			log.Error("failed init app", err)
			cancel()
		}

		// gRPC Server.
		go func() {
			address := fmt.Sprintf("%s:%d", cfg.Server.HOST, cfg.Server.GRPC_PORT)
			network := "tcp"
			l, err := net.Listen(network, address)
			if err != nil {
				log.Error("failed to listen to address ", err, " network ", network, " address ", address)
				cancel()
			}

			log.Info("🌏 start server...", " address ", address)

			defer func() {
				if err1 := l.Close(); err != nil {
					log.Error("failed to close", err1, "network", network, "address", address)
				}
			}()

			err = server.Serve(l)
			if err != nil {
				log.Error("failed start gRPC server", err, "network", network, "address", address)
				cancel()
			}
		}()

		// HTTP Server.
		go func() {
			mux := gwruntime.NewServeMux()

			gen.RegisterProfileServiceHandlerFromEndpoint(ctx, mux, "localhost:9090", []grpc.DialOption{grpc.WithInsecure()})
			gen.RegisterTaskServiceHandlerFromEndpoint(ctx, mux, "localhost:9090", []grpc.DialOption{grpc.WithInsecure()})

			s := &http.Server{
				Addr:    fmt.Sprintf("%s:%d", cfg.Server.HOST, cfg.Server.EXTERNAL_HTTP_PORT),
				Handler: cors(mux),
			}
			go func() {
				<-ctx.Done()
				log.Info("shutting down the http server")

				if err := s.Shutdown(context.Background()); err != nil {
					log.Error("failed to shutdown http server", err)
				}
			}()
			log.Info("start listening...", " address ", fmt.Sprintf("%s:%d", cfg.Server.HOST, cfg.Server.EXTERNAL_HTTP_PORT))

			if err := s.ListenAndServe(); errors.Is(err, http.ErrServerClosed) {
				log.Error("failed to listen and serve", err)
			}
		}()

		quit := make(chan os.Signal, 1)
		signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

		select {
		case v := <-quit:
			log.Info("signal.Notify", v)
		case done := <-ctx.Done():
			log.Info("ctx.Done", done)
		}
	},
}

func allowedOrigin(origin string) bool {
	if viper.GetString("cors") == "*" {
		return true
	}
	if matched, _ := regexp.MatchString(viper.GetString("cors"), origin); matched {
		return true
	}
	return false
}

func cors(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if allowedOrigin(r.Header.Get("Origin")) {
			w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization, ResponseType")
		}
		if r.Method == "OPTIONS" {
			return
		}
		h.ServeHTTP(w, r)
	})
}

func withLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info("Run request ", "http_method ", r.Method, " http_url ", r.URL)

		h.ServeHTTP(w, r)
	})
}

var allowedHeaders = map[string]struct{}{
	"x-request-id": {},
}

func isHeaderAllowed(s string) (string, bool) {
	// check if allowedHeaders contain the header
	if _, isAllowed := allowedHeaders[s]; isAllowed {
		// send uppercase header
		return strings.ToUpper(s), true
	}
	// if not in the allowed header, don't send the header
	return s, false
}

func init() {
	rootCmd.AddCommand(Cmd)
}
